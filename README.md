### Composer Lock Differ
CLI tool that, given two `composer.lock` files, will list packages that have been added, packages that have been removed, and packages that have changed versions.

## Usage

`npm i composer-lock-diff`

`node /path/to/composer-lock-diff.js <composer.lock.from> <composer.lock.from>`

Alternatively install globally `npm -g composer-lock-diff` and use as command: `composer-lock-diff.js <composer.lock.from> <composer.lock.from>`

```
Packages added
 - symfony/polyfill-intl-idn (v1.13.1)
 - symfony/css-selector (v3.4.37)
 - symfony/debug (v3.4.37)

Packages removed
 - symfony/polyfill-intl-idn (v1.13.1)
 - symfony/dependency-injection (v3.4.37)

Packages changed
 - composer/ca-bundle (1.2.5 -> 1.2.6)
 - composer/semver (1.5.0 -> 1.5.1)
 - doctrine/common (v2.11.0 -> 2.12.0)
 - doctrine/persistence (1.3.3 -> 1.3.6)
 - doctrine/reflection (v1.0.0 -> v1.1.0)
 - drupal/core (8.8.1 -> 8.8.2)
 - drupal/entity_browser (1.8.0 -> 1.10.0)
 - drupal/entity_browser_entity_form (1.8.0 -> 1.10.0)
 - egulias/email-validator (2.1.13 -> 2.1.15)
 - stack/builder (v1.0.5 -> v1.0.6)
 - stecman/symfony-console-completion (0.10.1 -> 0.11.0)
 - symfony/class-loader (v3.4.36 -> v3.4.37)
 - symfony/config (v3.4.36 -> v3.4.37)
 - symfony/console (v3.4.36 -> v3.4.37)
 - symfony/css-selector (v3.4.36 -> v3.4.37)
 - symfony/debug (v3.4.36 -> v3.4.37)
 - symfony/dom-crawler (v3.4.36 -> v3.4.37)
 - symfony/event-dispatcher (v3.4.36 -> v3.4.37)
 - symfony/filesystem (v3.4.36 -> v3.4.37)
 - symfony/finder (v3.4.36 -> v3.4.37)
 - symfony/http-foundation (v3.4.36 -> v3.4.37)
 - symfony/http-kernel (v3.4.36 -> v3.4.37)
 - symfony/process (v3.4.36 -> v3.4.37)
 - symfony/routing (v3.4.36 -> v3.4.37)
 - symfony/serializer (v3.4.36 -> v3.4.37)
 - symfony/translation (v3.4.36 -> v3.4.37)
 - symfony/validator (v3.4.36 -> v3.4.37)
 - symfony/var-dumper (v4.4.2 -> v4.4.4)
 - symfony/yaml (v3.4.36 -> v3.4.37)
 - typo3/phar-stream-wrapper (v3.1.3 -> v3.1.4)
 - phpspec/prophecy (1.10.1 -> v1.10.2)
 - symfony/browser-kit (v4.4.2 -> v4.4.4)
```

## Options
`--format, -f`: Output format.  Supports `plain` or `json`.  default: `plain`