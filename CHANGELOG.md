#### 1.2.2 (2020-02-06)

##### Documentation Changes

* **readme:**  Clarified usage (e1f477fd)

#### 1.2.1 (2020-02-06)

##### Bug Fixes

* **errors:**  Error handling (7f09360f)

### 1.2.0 (2020-02-06)

##### New Features

* **execution:**  Added bin entry to package.json (11f2630d)

### 1.1.0 (2020-02-06)

##### New Features

* **execution:**  Added hashbang to js file (e60b9d7f)

#### 1.0.1 (2020-02-06)

##### Documentation Changes

* **package:**  Added package details in package json (c8a0646c)
* **readme:**  Corrected grammar in readme (7d1c2d05)

